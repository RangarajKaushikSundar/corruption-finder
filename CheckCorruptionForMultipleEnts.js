//===================================== TRIGGER ===========================================//

// const ents = [{"entName":"test","entID":"test-ent-id"}];
// const report = await getReportForMultipleEnts(ents);

//===================================== PIPELINE FOR MULTIPLE ENTS ===========================================//

async function getReportForMultipleEnts(ents) {
  const allEntsDetails = [];
  const corruptedEntsCount = 0;
  for (let ent of ents) {
    try {
      const details = await checkEntForDuplicateStepKeys(ent.entID);
      allEntsDetails.push({
        ent,
        details
      });
      if(details.corruptedFlowsCount > 0) {
        corruptedEntsCount++;
      }
    } catch (e) {
      console.error("Could not capture details for ent ", ent);
    }
  }
  return {
    corruptedEntsCount,
    totalEntsCount: allEntsDetails.length,
    allEntsDetails
  };
}


//===================================== PIPELINE FOR SINGLE ENT ===========================================//


async function checkEntForDuplicateStepKeys(entID) {
  const flowsDetails = await getAllFlowsForEnt(entID);
  const corruptedFlowDetails = [];
  if (flowsDetails.length > 0) {
    for (let flow of flowsDetails) {
      if (flow.type !== 'text' && flow.type !== 'link' && flow.type != 'video') {
        const isCorrupted = await checkFlowHealth(entID, flow.type, flow.flowId);
        if (isCorrupted) {
          corruptedFlowDetails.push(flowsDetails)
        }
      }
    }
  }
  return {
    totalFlowsCount: flowsDetails.length,
    corruptedFlowsCount: corruptedFlowDetails.length,
    corruptedFlowDetails
  };
}


//===================================== ENTERPRISE ===========================================//

async function getAllFlowsForEnt(entId) {
  let url = `https://whatfix.com/api?service=content&action=top_dashboard&entid=${entId}`;
  let data = {
    "order_by": "published_at",
    "records": -1000,
    "indexer": entId,
    "source": "40",
    "ascending": false
  }

  const response = await fetch(url, {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json'
    }
  });

  const flowsList = await response.json();
  return filterFlowDetails(flowsList);
}
// ----- HELPERS ----- //
function filterFlowDetails(result) {
  const flowData = [];
  if (result && result.detail && result.detail.length > 0) {
    for (let entry of result.detail) {
      flowData.push({
        title: entry.title,
        type: entry.type,
        flowId: entry.flow_id
      });
    }
  }
  return flowData;
}
//===================================== FLOWS ===========================================//

async function checkFlowHealth(entId, type, flowId) { // returns boolean
  let url = generateURLForType(entId, type);
  let data = { "flow_id": flowId }

  const response = await fetch(url, {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json'
    }
  });

  const flowDetails = await response.json();
  const isCorrupted =  isFlowCorrupted(flowDetails);
  flowDetails = null;
  return isCorrupted;
}

// ----- HELPERS ----- //

function generateURLForType(entId, type) {
  let version = '';
  if (type === 'flow') {
    version = '&version=2'
  }
  const url = `https://whatfix.com/api?service=${type}&action=flowWithTags${version}&entid=${entId}`;
  return url;
}

function isFlowCorrupted(result) { // returns boolean
  if (result && result.detail && result.detail) {
    const detail = result.detail;
    const hasDuplicates =  hasDuplicateStepKey(detail);
    result = null;
    return hasDuplicates;
  }
}


function hasDuplicateStepKey(detail) { // returns boolean
  let mapper = {};
  for (let index = 0; index <= 50; index++) {
    const stepName = getStepKeyName(index); // step_1_key
    const stepValue = detail[stepName]; // timestamp
    const existingMapping = mapper[stepValue];
    const isDuplicateStepKey = (existingMapping !== undefined);
    if (isDuplicateStepKey) {
      return true;
    }
    if (stepValue !== undefined) {
      mapper[stepValue] = index;
    } else {
      return false;
    }
  }
  return false;
}


// ----- HELPERS ----- //
function getStepKeyName(step) {
  return `step_${step}_key`;
}
