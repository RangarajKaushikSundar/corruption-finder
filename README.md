# CORRUPTION FINDER #

This repository contains the following -

 * A list of all active, blocked and local enterprises
 * A script that can be run to find corruption for multiple ents.

## Steps for execution ##

* Login to a production enterprise. The `top_dashboard` and `flowWithTags` call need info from the cookie to work.
* Open Chrome Developer Tools => "Console" tab.
* Copy all code from "CheckCorruptionForMultipleEnts.js" file on to your console tab and hit "Enter"
* Select multiple ents from the active/blocked enterprises JSON file.
* Replace accordingly in this script and run it- 

```js
const ents = [{"entName":"test","entID":"test-ent-id"}]; // you should paste the list of enterprises here
const report = await getReportForMultipleEnts(ents);
```

## Some Stats ##

Going over some stats to show that I had underestimated on running this script over a weekend.

---
### Enterprise Counts ###
**Number of active ents** : 2230

**Number of blocked ents** : 2740

---

### Time ###
**Average time for `top_dashboard` call** : 2s

**Average time for `flowWithTags` call** : 4s

**Average time to run** :` (Number of ents *  2s) + (Number of flows * 4s) + n (constant computation time) ` **~ 3 weeks** _(considering all enterprises and an average of 100 flows/ent)_

---

### Size ###
**Average size of `top_dashboard` call's response** : 4KB

**Average size of `flowWithTags` call's response** : 13.4KB

**Average size to run** :` (Number of ents *  4KB) + (Number of flows * 13.4KB) + n (constant computation time) ` **~ 52 G8** _(considering all enterprises and an average of 100 flows/ent)_

---